﻿Public Class Player
    Inherits Character

    Public Overrides Property BackColor As Color = Color.Black
    Public Property Energy As UShort = 100

    Private keyPressedLeft As Boolean = False
    Private keyPressedRight As Boolean = False
    Private keyPressedUp As Boolean = False
    Private keyPressedDown As Boolean = False
    Private keyPressedShift As Boolean = False
    Private keyPressedEsc As Boolean = False

    Protected Overrides Sub Initiate()
        Location = New Point(360, 290)
    End Sub

    Public Sub HandleKeyDown(e As KeyEventArgs)
        Select Case e.KeyCode.ToString()
            Case "A"
                keyPressedLeft = True
            Case "D"
                keyPressedRight = True
            Case "W"
                keyPressedUp = True
            Case "S"
                keyPressedDown = True
            Case "Escape"
                keyPressedEsc = True
        End Select

        If e.Shift Then
            keyPressedShift = True
        End If
    End Sub
    Public Sub HandleKeyUp(e As KeyEventArgs)
        Select Case e.KeyCode.ToString()
            Case "A"
                keyPressedLeft = False
            Case "D"
                keyPressedRight = False
            Case "W"
                keyPressedUp = False
            Case "S"
                keyPressedDown = False
            Case "Escape"
                If keyPressedEsc Then
                    Runaway.tmrGameTick.Enabled = False
                    Runaway.tmrPlyTick.Enabled = False

                    MsgBox("The game is paused, press Ok to continue.", MsgBoxStyle.OkOnly, "Game Paused")

                    Runaway.tmrGameTick.Enabled = True
                    Runaway.tmrPlyTick.Enabled = True

                    keyPressedEsc = False
                End If
        End Select

        If Not e.Shift Then
            keyPressedShift = False
        End If
    End Sub

    Public Overrides Sub Tick()
        Dim moveAmount As UShort = Runaway.intMoveAmount

        If keyPressedShift And Energy > 4 Then
            moveAmount = moveAmount * 2
            Energy -= 4
        ElseIf Energy < 100 Then
            Energy += 1
        End If

        If keyPressedUp Then
            Top -= moveAmount
        End If

        If keyPressedLeft Then
            Left -= moveAmount
        End If

        If keyPressedDown Then
            Top += moveAmount
        End If

        If keyPressedRight Then
            Left += moveAmount
        End If

        moveBackInBounds()

        Runaway.intScoreMultiplier = 1

        For Each Enemy In Runaway.Enemies
            If (Math.Abs(Enemy.Top - Top) + Math.Abs(Enemy.Left - Left) < Runaway.intMoveAmount * 10) Then
                Runaway.intScoreMultiplier += 1
            End If
        Next
    End Sub
End Class
