﻿Public Class Character
    Inherits PictureBox

    Public Overrides Property BackColor As Color = Color.Green

    Private pntInitialPos As Point
    Private intTickNo As ULong = 0

    Public Sub New()
        Size = New Drawing.Size(20, 20)
        Initiate()
    End Sub

    Protected Overridable Sub Initiate()
        Location = New Point(Math.Round(Int((725 * Rnd()) + 1) / 10) * 10, Math.Round(Int((560 * Rnd()) + 1) / 10) * 10)
        pntInitialPos = Location
    End Sub

    Public Overridable Sub Tick()
        Location = New Point(Math.Cos(intTickNo) * 10 + pntInitialPos.X, Math.Sin(intTickNo) * 10 + pntInitialPos.Y)
        intTickNo += 1
    End Sub

    Protected Sub moveBackInBounds()
        If (Location.Y > 525) Then
            Location = New Point(Location.X, 525)
        ElseIf (Location.Y < 0) Then
            Location = New Point(Location.X, 0)
        End If

        If (Location.X > 725) Then
            Location = New Point(725, Location.Y)
        ElseIf (Location.X < 0) Then
            Location = New Point(0, Location.Y)
        End If
    End Sub
End Class
