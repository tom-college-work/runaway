﻿Public Class Runaway
    Public Player As Player
    Public Enemies As List(Of Enemy) = New List(Of Enemy)
    Public intMoveAmount As UShort = 5
    Public intScoreMultiplier As UShort = 1

    Private Score As Integer = 0

    Private Sub Runaway_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Player = New Player()
        Me.Controls.Add(Player)

        Enemies.Add(New Enemy())
        Me.Controls.Add(Enemies.Last())
    End Sub

    Private Sub Runaway_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        Player.HandleKeyDown(e)
    End Sub

    Private Sub Runaway_KeyUp(sender As Object, e As KeyEventArgs) Handles MyBase.KeyUp
        Player.HandleKeyUp(e)
    End Sub

    Private Sub tmrGameTick_Tick(sender As Object, e As EventArgs) Handles tmrGameTick.Tick
        For Each Enemy In Enemies
            Enemy.Tick()

            If (Player.Bounds.IntersectsWith(Enemy.Bounds)) Then
                tmrGameTick.Enabled = False
                tmrPlyTick.Enabled = False
                If MsgBox("You ended with a total score of " & Score.ToString() & ", would you like to retry?", MessageBoxButtons.YesNo, "Game Over") = MsgBoxResult.Yes Then
                    Score = 0

                    Me.Controls.Remove(Player)

                    For Each removeEnemy In Enemies
                        Me.Controls.Remove(removeEnemy)
                    Next

                    Enemies = New List(Of Enemy)

                    Player = New Player()
                    Me.Controls.Add(Player)

                    Enemies.Add(New Enemy())
                    Me.Controls.Add(Enemies.Last())

                    tmrGameTick.Enabled = True
                    tmrPlyTick.Enabled = True
                    Exit Sub
                Else
                    Me.Close()
                End If
                Exit Sub
            End If
        Next

        Score += intScoreMultiplier
        lblScore.Text = "Score: " & Score.ToString()

        Select Case Score
            Case 250
                Enemies.Add(New Enemy())
                Me.Controls.Add(Enemies.Last())
            Case 500
                Enemies.Add(New Enemy())
                Me.Controls.Add(Enemies.Last())
            Case 1000
                For i = 1 To 2
                    Enemies.Add(New Enemy())
                    Me.Controls.Add(Enemies.Last())
                Next
            Case 2000
                For i = 1 To 4
                    Enemies.Add(New Enemy())
                    Me.Controls.Add(Enemies.Last())
                Next
            Case 2500
                For i = 1 To 5
                    Enemies.Add(New Enemy())
                    Me.Controls.Add(Enemies.Last())
                Next
            Case 3000
                tmrGameTick.Interval = 50
            Case 5000
                For i = 1 To 6
                    Enemies.Add(New Enemy())
                    Me.Controls.Add(Enemies.Last())
                Next
            Case 10000
                tmrGameTick.Interval = 30
        End Select
    End Sub

    Private Sub tmrPly_Tick(sender As Object, e As EventArgs) Handles tmrPlyTick.Tick
        Player.Tick()
        prgEnergy.Value = Player.Energy
    End Sub
End Class
