﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Runaway
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.tmrGameTick = New System.Windows.Forms.Timer(Me.components)
        Me.lblScore = New System.Windows.Forms.Label()
        Me.tmrPlyTick = New System.Windows.Forms.Timer(Me.components)
        Me.prgEnergy = New System.Windows.Forms.ProgressBar()
        Me.lblEnergy = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tmrGameTick
        '
        Me.tmrGameTick.Enabled = True
        '
        'lblScore
        '
        Me.lblScore.AutoSize = True
        Me.lblScore.BackColor = System.Drawing.SystemColors.GrayText
        Me.lblScore.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScore.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblScore.Location = New System.Drawing.Point(0, 550)
        Me.lblScore.Name = "lblScore"
        Me.lblScore.Size = New System.Drawing.Size(80, 25)
        Me.lblScore.TabIndex = 1
        Me.lblScore.Text = "Score: "
        '
        'tmrPlyTick
        '
        Me.tmrPlyTick.Enabled = True
        Me.tmrPlyTick.Interval = 35
        '
        'prgEnergy
        '
        Me.prgEnergy.Location = New System.Drawing.Point(638, 551)
        Me.prgEnergy.Name = "prgEnergy"
        Me.prgEnergy.Size = New System.Drawing.Size(100, 23)
        Me.prgEnergy.TabIndex = 2
        '
        'lblEnergy
        '
        Me.lblEnergy.AutoSize = True
        Me.lblEnergy.BackColor = System.Drawing.SystemColors.GrayText
        Me.lblEnergy.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblEnergy.Location = New System.Drawing.Point(595, 554)
        Me.lblEnergy.Name = "lblEnergy"
        Me.lblEnergy.Size = New System.Drawing.Size(43, 13)
        Me.lblEnergy.TabIndex = 3
        Me.lblEnergy.Text = "Energy:"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.SystemColors.GrayText
        Me.PictureBox1.Location = New System.Drawing.Point(-5, 544)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(756, 38)
        Me.PictureBox1.TabIndex = 4
        Me.PictureBox1.TabStop = False
        '
        'Runaway
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(744, 581)
        Me.Controls.Add(Me.lblEnergy)
        Me.Controls.Add(Me.prgEnergy)
        Me.Controls.Add(Me.lblScore)
        Me.Controls.Add(Me.PictureBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "Runaway"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Runaway"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tmrGameTick As Timer
    Friend WithEvents lblScore As Label
    Friend WithEvents tmrPlyTick As Timer
    Friend WithEvents prgEnergy As ProgressBar
    Friend WithEvents lblEnergy As Label
    Friend WithEvents PictureBox1 As PictureBox
End Class
