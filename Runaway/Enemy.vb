﻿Public Class Enemy
    Inherits Character
    Public Overrides Property BackColor As Color = Color.Red

    Public Overrides Sub Tick()
        If (Runaway.Player.Top > Top) Then
            moveDirection(Top, False)
        ElseIf (Runaway.Player.Top < Top) Then
            moveDirection(Top, True)
        End If

        If (Runaway.Player.Left > Left) Then
            moveDirection(Left, False)
        ElseIf (Runaway.Player.Left < Left) Then
            moveDirection(Left, True)
        End If
    End Sub

    Private Sub moveDirection(ByRef axis As Integer, invert As Boolean)
        If (invert) Then
            axis -= Runaway.intMoveAmount
        Else

            axis += Runaway.intMoveAmount
        End If

        For Each enemy In Runaway.Enemies
            If (Not enemy.Location = Location And enemy.Bounds.IntersectsWith(Bounds)) Then
                If (invert) Then
                    axis += Runaway.intMoveAmount
                    tryMove(enemy)
                Else
                    axis -= Runaway.intMoveAmount
                    tryMove(enemy)
                End If
            End If
        Next
    End Sub

    Private Sub tryMove(target As Enemy)
        While target.Bounds.IntersectsWith(Bounds)
            If (Int((2 * Rnd()) + 1) = 1) Then
                If (Int((2 * Rnd()) + 1) = 1) Then
                    Top += Runaway.intMoveAmount
                Else
                    Top -= Runaway.intMoveAmount
                End If
            Else
                If (Int((2 * Rnd()) + 1) = 1) Then
                    Left += Runaway.intMoveAmount
                Else
                    Left -= Runaway.intMoveAmount
                End If
            End If
        End While
    End Sub
End Class
